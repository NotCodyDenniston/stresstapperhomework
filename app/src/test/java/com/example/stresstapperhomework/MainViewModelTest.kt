package com.example.stresstapperhomework

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

internal class MainViewModelTest{

private val mainViewModel = MainViewModel()

@Test
@DisplayName("Test the reset functionality")
fun resetTest(){
    mainViewModel.randomNumber()
    mainViewModel.reset()
    Assertions.assertEquals(255, mainViewModel.red.value)
    Assertions.assertEquals(255, mainViewModel.blue.value)
    Assertions.assertEquals(255, mainViewModel.green.value)
}

@Test
@DisplayName("Randomizes the color")
fun randomizeTest(){
    for (i in 1..10){
    mainViewModel.randomNumber()
    Assertions.assertTrue( mainViewModel.red.value>= 0 && mainViewModel.red.value < 256)
    Assertions.assertTrue( mainViewModel.green.value>= 0 && mainViewModel.green.value < 256)
    Assertions.assertTrue( mainViewModel.blue.value>= 0 && mainViewModel.blue.value < 256)

    }
}

}