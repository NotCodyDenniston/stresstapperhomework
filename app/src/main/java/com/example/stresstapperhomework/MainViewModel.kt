package com.example.stresstapperhomework

import android.util.Log
import androidx.compose.ui.graphics.Color
import androidx.lifecycle.ViewModel
import kotlin.random.Random
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

class MainViewModel : ViewModel() {

     val red = MutableStateFlow(0)

    val green = MutableStateFlow(0)

     val blue = MutableStateFlow(0)

fun randomNumber() {
    red.value = Random.nextInt(0,255)
    green.value = Random.nextInt(0,255)
    blue.value = Random.nextInt(0,255)
}

fun reset(){
    red.value = 255
    green.value = 255
    blue.value = 255
}
}