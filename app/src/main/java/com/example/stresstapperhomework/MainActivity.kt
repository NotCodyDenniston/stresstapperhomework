package com.example.stresstapperhomework

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.combinedClickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.stresstapperhomework.ui.theme.StressTapperHomeworkTheme
import kotlin.random.Random

class MainActivity : ComponentActivity() {
    private val mainViewModel by viewModels<MainViewModel>()
    @OptIn(ExperimentalFoundationApi::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            StressTapperHomeworkTheme {
                val randomColor = remember { mutableStateOf(Color(255,255,255,255)) }
                Surface(
                    modifier = Modifier
                        .background(color = randomColor.value)
                        .fillMaxSize()
                        .wrapContentSize(Alignment.Center),
                   color = MaterialTheme.colorScheme.background
                )
                {
                   Column(
                       horizontalAlignment = Alignment.CenterHorizontally,
                       verticalArrangement = Arrangement.Center,
                       modifier = Modifier.background(color = randomColor.value)
                   ) {
                       Box(

                       ) {
                           Surface(modifier = Modifier
                               .combinedClickable(
                                   onClick = {
                                       mainViewModel.randomNumber()
                                       randomColor.value = Color(
                                           mainViewModel.red.value,
                                           mainViewModel.green.value,
                                           mainViewModel.blue.value,255) },
                                   onLongClick = {
                                       mainViewModel.reset()
                                       randomColor.value = Color(
                                           mainViewModel.red.value,
                                           mainViewModel.green.value,
                                           mainViewModel.blue.value,255) }
                               )
                               .padding(10.dp)
                               .clip(CircleShape)
                               .size(180.dp),
                               color = Color.Green
                           ) {
                               Text(
                                   modifier = Modifier
                                       .wrapContentSize(Alignment.Center)
                                       .padding(20.dp),
                                   text = "HOLD AND RESET MEEEE",
                                   fontSize = 20.sp,

                                   )

                           }
                       }
//                       Box() {
//
//                           Surface(
//                               modifier = Modifier.combinedClickable(
//                                   onClick = {
//                                             randomColor.value = Color(Random.nextInt(0,255), Random.nextInt(0, 255),Random.nextInt(0, 255),255 )
//                                   },
//                                   onLongClick = {
//                                       randomColor.value = Color.Blue
//                                   }
//                               )
//                                   .background(Color.Green)
//                                   .size(200.dp)
//                                   .clip(CircleShape)
//                           ) {
//                            Text(text = "Click Me!")
//                           }
//                       }

                   }
                }
            }
        }
    }
}

